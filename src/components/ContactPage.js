import React from 'react';
import contact from './images/contacts-1.png';
import vintage from './images/VINtage.png';
import facebook from './images/facebook.png';
import linkedin from './images/linkedin.png';
import instagram from './images/instagram.png';
import ymail from './images/ymail.png';
import gmail from './images/gmail2.png';
import phone from './images/phone.png';
import videoBG from './videos/Video-bg1.mp4';
import AOS from 'aos';
import '../../node_modules/aos/dist/aos.css'; 


function ContactPage(){
	return(
		<section id="contacts">
			<div id="contact2"></div>
			<div className="container">
				<img src={contact}  data-aos="fade-down"/>
				<div className="row">
					<div className="col-10 text-center offset-1 mt-3 mb-3">
						<div data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
							<img src={vintage} className="logo-name img-fluid"/>
						</div>
					</div>
					<div className=" col-lg-12 col-md-4 col-sm-10 d-lg-flex offset-sm-1 justify-content-around mt-3">
						<div className=" icon-box" data-aos="fade-up" data-aos-delay="300">
							<a className="text-white" href="https://web.facebook.com/JagerFaust08/" target="_blank">
							<img src={facebook} className="cont-icon img-fluid"/><span className="text-white ml-1 font-weight-bold">Muvslyk Jager</span></a>
						</div>
						<div className=" icon-box" data-aos="fade-up" data-aos-delay="400">
							<a className="text-white" href="https://www.linkedin.com/in/arvin-lobedesis-b708a196/" target="_blank">
							<img src={linkedin} className="cont-icon img-fluid"/><span className="text-white ml-1 font-weight-bold">Arvin Lobedesis</span></a>
						</div>
						<div className=" icon-box" data-aos="fade-up" data-aos-delay="500">
							<a className="text-white" href="https://www.instagram.com/jagerfaust/?hl=en" target="_blank">
							<img src={instagram} className="cont-icon img-fluid"/><span className="text-white ml-1 font-weight-bold">jagerfaust</span></a>
						</div>
						
					</div>
					<div className=" col-lg-12 col-md-4 col-sm-10 d-lg-flex offset-sm-1 justify-content-around mt-3">
						<div className=" icon-box" data-aos="fade-up" data-aos-delay="600">
							<a href="#contact2" className="text-white"><img src={ymail} className="cont-icon img-fluid"/><span className="text-white ml-1 font-weight-bold">jager_faust@yahoo.com</span>
							</a>
						</div>
						<div className=" icon-box" data-aos="fade-up" data-aos-delay="700">
							<a href="#contact2" className="text-white"><img src={gmail} className="cont-icon img-fluid"/><span className="text-white ml-1 font-weight-bold">jagergallant@gmail.com</span>
							</a>
						</div>
						<div className=" icon-box" data-aos="fade-up" data-aos-delay="800">
							<a href="#contact2" className="text-white"><img src={phone} className="cont-icon img-fluid"/><span className="text-white ml-1 font-weight-bold">09950531880</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<footer><p className="text-white mt-5">© 2020 VINtage Art | Muvslyk Jager <br/>All Rights Reserved </p></footer>
			<video autoPlay muted loop id="bgVid1" className="d-none d-lg-block"><source src={videoBG}  type="video/mp4"/></video>
		</section>
		)
}
export default ContactPage;