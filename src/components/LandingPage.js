import React from 'react';
import {Link} from 'react-router-dom';
import welcome from './images/welcome.png';
import logo from './images/vintageart.jpg';
import landingBg from './images/landing-bg2.jpg';
import skillDark from './images/skill-dark-new.png';
import skillLight from './images/skill-light-new.png';
import skillSmLight from './images/skill-sm-light.png';
import skills from './images/skills.png';
import projDark from './images/project-dark-new.png';
import projLight from './images/project-light-new.png';
import projSmLight from './images/project-sm-light.png';
import projects from './images/projects.png';
import contactDark from './images/contact-dark-new.png';
import contactLight from './images/contact-light-new.png';
import contactSmLight from './images/contact-sm-light.png';
import contacts from './images/contacts.png';
import videoBG from './videos/Video-bg1.mp4';
import AOS from 'aos';
import '../../node_modules/aos/dist/aos.css';


function LandingPage(){
	return(
		<section id="landing">
			<div className="container">
				<div className="row">
					<img src={welcome} id="welcome" className="img-fluid d-md-none d-lg-block" data-aos="fade-down"  data-aos-duration="1000"/>
					<img src={welcome} id="welcome2" className="img-fluid d-lg-none d-none d-md-block" data-aos="fade-down"  data-aos-duration="1000"/>
					<Link to="/profile">
					<a href=""><img src={logo} className="rounded-circle img-fluid d-md-none d-lg-block" height="250px" id="logo"  data-aos="fade-down"/></a>
					<a href=""><img src={logo} className="rounded-circle img-fluid d-lg-none d-none d-md-block" height="250px" id="logo2"  data-aos="fade-down"/></a>
					</Link>
					<div className="col-lg-12 d-none d-lg-block">
						<div className="box" data-aos="fade-down"  data-aos-duration="500" data-aos-delay="300">
							<Link to="/skill">
							<a href="#skill2"><img src={skillDark} className="img-bot"/></a>
							<a href="#skill2"><img src={skillLight} className="img-top"/></a>
							<a href="#skill2"><img src={skills} className="img-txt"/></a>
							</Link>
						</div>
						<div className="box2" data-aos="fade-down"  data-aos-duration="600" data-aos-delay="500">
							<Link to="/project">
							<a href=""><img src={projDark} className="img-bot"/></a>
							<a href=""><img src={projLight} className="img-top"/></a>
							<a href=""><img src={projects} className="img-txt"/></a>
							</Link>
						</div>
						<div className="box3" data-aos="fade-down"  data-aos-duration="700" data-aos-delay="700">
							<Link to="/contact">
							<a href=""><img src={contactDark} className="img-bot"/></a>
							<a href=""><img src={contactLight} className="img-top"/></a>
							<a href=""><img src={contacts} className="img-txt"/></a>
							</Link>
						</div>
					</div>
					<div className="d-lg-none d-none d-md-block box-1">
						<div className="row text-center">
							<div data-aos="fade-down"  data-aos-duration="500" data-aos-delay="300">
								<Link to="/skill">
								<a href=""><img src={skillSmLight} className="rounded-circle icon2"/></a>
								</Link>
							</div>
						</div>
					</div>
					<div className="d-lg-none d-none d-md-block box-2">
						<div className="row text-center">
							<div data-aos="fade-down"  data-aos-duration="600" data-aos-delay="400">
								<Link to="/project">
								<a href=""><img src={projSmLight} className="rounded-circle icon2"/></a>
								</Link>
							</div>
						</div>
					</div>
					<div className="d-lg-none d-none d-md-block box-3">
						<div className="row text-center">
							<div data-aos="fade-down"  data-aos-duration="700" data-aos-delay="500">
								<Link to="/contact">
								<a href=""><img src={contactSmLight} className="rounded-circle icon2"/></a>
								</Link>
							</div>
						</div>
					</div>
				</div>
			</div>
			<video autoPlay muted loop id="bgVid1" className="d-none d-lg-block"><source src={videoBG}  type="video/mp4"/></video>
		</section>
		)
}

export default LandingPage;