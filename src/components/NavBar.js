import React from 'react';
import {Link} from 'react-router-dom';
import AOS from 'aos';
import '../../node_modules/aos/dist/aos.css'; 
import logo2 from './images/vintageart2.png';
import skillSmLight from './images/skill-sm-light.png';
import projectSmLight from './images/project-sm-light.png';
import contactSmLight from './images/contact-sm-light.png';
import topSmLight from './images/top-sm-light.png';

let navStyle = {
	backgroundColor: 'black',
	
}

function NavBar(){
	return(
	<>
		<nav style={navStyle} className="navbar navbar-expand-lg navbar-dark d-lg-none" id="nav2">
		  	
			<div className="box4-1">
				<Link to="/profile">
				<a href=""><img src={logo2} className="rounded-circle icon " data-aos="flip-right" data-aos-delay="200" data-toggle="tooltip" data-placement="right" title="Profile"/></a>
				</Link>
			</div>
			<div className="box4-1">
				<Link to="/skill">
				<a href=""><img src={skillSmLight} className="rounded-circle icon " data-aos="flip-right" data-aos-delay="300"data-toggle="tooltip" data-placement="right" title="Skills"/></a>
				</Link>
			</div>
			<div className="box4-1">
				<Link to="/project">
				<a href=""><img src={projectSmLight} className="rounded-circle icon " data-aos="flip-right" data-aos-delay="400" data-toggle="tooltip" data-placement="right" title="Projects"/></a>
				</Link>
			</div>
			<div className="box4-1">
				<Link to="/contact">
				<a href=""><img src={contactSmLight} className="rounded-circle icon " data-aos="flip-right" data-aos-delay="500" data-toggle="tooltip" data-placement="right" title="Contacts"/></a>
				</Link>
			</div>
			<div className="box4-1">
				<Link to="/">
				<a href=""><img src={topSmLight} className="icon rounded-circle" data-aos="flip-right" data-aos-delay="500" data-toggle="tooltip" data-placement="right" title="Home"/></a>
				</Link>
			</div>
		  
		</nav>
		<nav className="d-none d-lg-block">
			<div className="box4">
				<Link to="/profile">
				<a href=""><img src={logo2} className="rounded-circle icon ml-2" data-aos="flip-right" data-aos-delay="200" data-toggle="tooltip" data-placement="right" title="Profile"/></a>
				</Link>
			</div>
			<div className="box4">
				<Link to="/skill">
				<a href=""><img src={skillSmLight} className="rounded-circle icon mt-2 ml-2" data-aos="flip-right" data-aos-delay="300"data-toggle="tooltip" data-placement="right" title="Skills"/></a>
				</Link>
			</div>
			<div className="box4">
				<Link to="/project">
				<a href=""><img src={projectSmLight} className="rounded-circle icon mt-3 ml-2" data-aos="flip-right" data-aos-delay="400" data-toggle="tooltip" data-placement="right" title="Projects"/></a>
				</Link>
			</div>
			<div className="box4">
				<Link to="/contact">
				<a href=""><img src={contactSmLight} className="rounded-circle icon mt-4 ml-2" data-aos="flip-right" data-aos-delay="500" data-toggle="tooltip" data-placement="right" title="Contacts"/></a>
				</Link>
			</div>
			<div className="divide"></div>
			<div className="box4">
				<Link to="/">
				<a href=""><img src={topSmLight} className="icon rounded-circle ml-2" data-aos="flip-right" data-aos-delay="500" data-toggle="tooltip" data-placement="right" title="Home"/></a>
				</Link>
			</div>
		</nav>
	</>
		)
}AOS.init(); 

export default NavBar;