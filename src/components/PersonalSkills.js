import React from 'react';


function PersonalSkills(){
	return(
		<div className="row">
			<div className="col-6 text-center d-lg-flex d-md-flex justify-content-around mt-5">
				<div className="mx-3 red" data-aos="fade-down" data-aos-delay="200">
					<i className="fas fa-palette fa-4x"></i>
					<p>Creativity</p>
				</div>
				<div className="mx-3 green" data-aos="fade-down" data-aos-delay="300">
					<i className="fas fa-comments fa-4x"></i>
					<p>Communication</p>
				</div>
				<div className="mx-3 blue" data-aos="fade-down" data-aos-delay="400">
					<i className="fas fa-cogs fa-4x"></i>
					<p>Collaboration</p>
				</div>
			</div>
			<div className="col-6 text-center  d-lg-flex d-md-flex justify-content-around mt-5">
				<div className="mx-3 yellow" data-aos="fade-down" data-aos-delay="500">
					<i className="fas fa-brain fa-4x"></i>
					<p>Critical Thinking</p>
				</div>
				<div className="mx-3 salmon" data-aos="fade-down" data-aos-delay="600">
					<i className="fas fa-tasks fa-4x"></i>
					<p>Multi-tasking</p>
				</div>
				<div className="mx-3 violet" data-aos="fade-down" data-aos-delay="700">
					<i className="fas fa-arrows-alt fa-4x"></i>
					<p>Adaptability</p>
				</div>
			</div>
		</div>
		)
}
export default PersonalSkills;