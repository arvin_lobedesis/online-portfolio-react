import React from 'react';
import profile from './images/profile-1.png';
import vin from './images/vin.jpg';
import videoBG from './videos/Video-bg1.mp4';
import AOS from 'aos';
import '../../node_modules/aos/dist/aos.css'; 


function ProfilePage(){
	return(
		<section id="about">
			<div className="container">
				<img src={profile}  data-aos="fade-down"/>
				<div className="container" id="about3">
					<div className="row mt-5">
						<div className="aboutLeft col-sm-5 col-md-6 col-lg-3 offset-md-3 offset-lg-2" data-aos="fade-down" data-aos-duration="1000">
							<img src={vin} className="img-fluid" id="vin"/>
						</div>
						<div className="aboutRight col-sm-10 col-md-10 col-lg-6 offset-md-1 " data-aos="fade-up" data-aos-duration="1000">
							<h1 className="text-white display-5 font-weight-bold">HI! I'M JAGER</h1>
							<h4 className="text-warning"><strong>ARVIN EMMANUEL LOBEDESIS</strong></h4>
							<h6 className="text-white"><strong>Graphic Artist | Multimedia Artist | Web Developer</strong></h6>
							<hr className="bg-dark"/>
							<h6 className="text-white my-3 txt-desc">My hobbies are drawing, playing computer and mobile games, watching movies and anime. I also love to  travel and go on an adventure with my friends and loved ones.</h6>
							<hr className="bg-dark"/>
							<p className="text-white">"The Lord is my light and my salvation－whom shall I fear?" <br/>~ Psalm 27:1</p>
						</div>
						
					</div>
				</div>
			</div>
			<video autoPlay muted loop id="bgVid1" className="d-none d-lg-block"><source src={videoBG}  type="video/mp4"/></video>
		</section>
		)
}AOS.init(); 

export default ProfilePage;