import React from 'react';
import projects from './images/projects-1.png';
import carli from './images/carli-new.png';
import cotd from './images/cotd-new.png';
import artesan from './images/artesan-new.png';
import projectSub from './images/project-sub-new.png';
import videoBG from './videos/Video-bg1.mp4';
import AOS from 'aos';
import '../../node_modules/aos/dist/aos.css'; 


function ProjectPage(){
	return(
		<section id="projects">
			<div className="container">
				<div id="project2"></div>
				<img src={projects}  data-aos="fade-down"/>
				<div className="row">
					<div className=" col-lg-10 col-md-4 col-sm-10 d-lg-flex offset-lg-1 offset-sm-1 offset-1 justify-content-around mt-2">
						<div data-aos="fade-down" data-aos-delay="300" className="project-box">
							<p className="text-white">Project: Carliani's Bakeshop</p>
							<a href="https://arvin_lobedesis.gitlab.io/capstone-1" target="_blank">
							<img src={carli} className="img-fluid project-item"/></a>
						</div>
						<div data-aos="fade-down" data-aos-delay="400" className="project-box">
							<p className="text-white">Project: COTD</p>
							<a href="https://arvin_lobedesis.gitlab.io/color-of-the-day/" target="_blank">
							<img src={cotd} className="img-fluid project-item"/></a>
						</div>
						<div data-aos="fade-down" data-aos-delay="500" className="project-box">
							<p className="text-white">Project: Arte-san</p>
							<a href="http://enigmatic-bayou-20755.herokuapp.com/" target="_blank">
							<img src={artesan} className="img-fluid project-item"/></a>
						</div>
					</div>
					<div className=" col-lg-10 col-md-4 col-sm-10 d-lg-flex offset-lg-1 offset-sm-1 offset-1 justify-content-around mt-2">
						<div data-aos="fade-down" data-aos-delay="600" className="project-box">
							<p className="text-white">New Project</p>
							<img src={projectSub} className="img-fluid project-item"/>
						</div>
						<div data-aos="fade-down" data-aos-delay="700" className="project-box">
							<p className="text-white">New Project</p>
							<img src={projectSub} className="img-fluid project-item"/>
						</div>
						<div data-aos="fade-down" data-aos-delay="800" className="project-box mb-5">
							<p className="text-white">New Project</p>
							<img src={projectSub} className="img-fluid project-item"/>
						</div>
					</div>
				</div>
			</div>
			<video autoPlay muted loop id="bgVid1" className="d-none d-lg-block"><source src={videoBG}  type="video/mp4"/></video>
		</section>
		)
}
export default ProjectPage;