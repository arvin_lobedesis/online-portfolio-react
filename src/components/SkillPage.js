import React from 'react';
import skills from './images/skills-1.png';
import videoBG from './videos/Video-bg1.mp4';
import TechSkills from './TechSkills';
import PersonalSkills from './PersonalSkills';

function SkillPage(){
	return(
		<section id="skills">
			
		
			<div id="skill2"></div>
			<div className="container">
				<img src={skills}  data-aos="fade-down"/>
				<div className="row">
					<div className="col-12 ">
						<h4 className="text-center mt-5 text-white" data-aos="fade-down">Personal Skills</h4>
						<PersonalSkills/>
					</div>
					<div className="col-12 mb-4">
						<h4 className="text-center mt-3 text-white" data-aos="fade-up">Technical Skills</h4>
						<TechSkills/>
					</div>
							
				</div>
			</div>
			<video autoPlay muted loop id="bgVid1" className="d-none d-lg-block"><source src={videoBG}  type="video/mp4"/></video>
		</section>
				
		

		)
}
export default SkillPage;