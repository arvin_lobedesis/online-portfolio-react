import React from 'react';
import html from './images/html.png';
import css from './images/css.png';
import js from './images/js.png';
import bootstrap from './images/bootstrap.png';
import gitlab from './images/gitlab.png';
import laravel from './images/laravel.png';
import php from './images/php.png';
import mysql from './images/mysql.png';
import react from './images/react.png';
import mongodb from './images/mongodb.png';
import express from './images/express.png';
import node from './images/node.png';
import psd from './images/psd.png';
import illustrator from './images/illustrator.png';
import indesign from './images/indesign.png';
import premiere from './images/premiere.png';


function TechSkills(){
	return(
	
		<div className="row">
			<div className="col-lg-3 col-6 text-center text-white mt-3">
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="200">
					<img src={html} className="logo-icon img-fluid"/>
					<p className="txt-logo">HTML</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="300">
					<img src={css} className="logo-icon img-fluid" />
					<p className="txt-logo">CSS</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="400">
					<img src={js} className="logo-icon img-fluid" />
					<p className="txt-logo">Javascript</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="500">
					<img src={bootstrap} className="logo-icon img-fluid"/>
					<p className="txt-logo">Bootstrap</p>
				</div>
			</div>
			<div className="col-lg-3 col-6 text-center text-white mt-3">
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="200">
					<img src={gitlab} className="logo-icon img-fluid"/>
					<p className="txt-logo">Gitlab</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="300">
					<img src={laravel} className="logo-icon img-fluid"/>
					<p className="txt-logo">Laravel</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="400">
					<img src={php} className="logo-icon img-fluid"/>
					<p className="txt-logo">PHP</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="500">
					<img src={mysql} className="logo-icon img-fluid"/>
					<p className="txt-logo">MySQL</p>
				</div>
			</div>
			<div className="col-lg-3 col-6 text-center text-white mt-3">
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="200">
					<img src={react} className="logo-icon img-fluid"/>
					<p className="txt-logo">React</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="300">
					<img src={mongodb} className="logo-icon img-fluid"/>
					<p className="txt-logo">MongoDB</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="400">
					<img src={express} className="logo-icon img-fluid"/>
					<p className="txt-logo">Express</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="500">
					<img src={node} className="logo-icon img-fluid"/>
					<p className="txt-logo">Node</p>
				</div>
			</div>
			<div className="col-lg-3 col-6 text-center text-white mt-3">
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="200">
					<img src={psd} className="logo-icon img-fluid"/>
					<p className="txt-logo">Photoshop</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="300">
					<img src={illustrator} className="logo-icon img-fluid"/>
					<p className="txt-logo">Illustrator</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="400">
					<img src={indesign} className="logo-icon img-fluid"/>
					<p className="txt-logo">InDesign</p>
				</div>
				<div className="mx-3 logo-box" data-aos="fade-up" data-aos-delay="500">
					<img src={premiere} className="logo-icon img-fluid"/>
					<p className="txt-logo">Premiere</p>
				</div>
			</div>
		</div>
		
	
		)
}
export default TechSkills;