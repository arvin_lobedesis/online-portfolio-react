import React from 'react';
import ReactDOM from 'react-dom';
import './app.css'
import LandingPage from './components/LandingPage';
import ProfilePage from './components/ProfilePage';
import SkillPage from './components/SkillPage';
import ProjectPage from './components/ProjectPage';
import ContactPage from './components/ContactPage';
import NavBar from './components/NavBar';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

const DefaultContainer = ()=>(
	<div>
		<NavBar/>
		<Switch>
			<Route exact path="/profile" component={ProfilePage}/>
			<Route exact path="/skill" component={SkillPage}/>
			<Route exact path="/project" component={ProjectPage}/>
			<Route exact path="/contact" component={ContactPage}/>
		</Switch>


	</div>
)
const myComponent = (
	<BrowserRouter>
		
		<Switch>
			<Route exact path="/" component={LandingPage}/>
			<Route component={DefaultContainer}/>
		</Switch>
	</BrowserRouter>
)

const divRoot = document.getElementById('root')

ReactDOM.render(myComponent, divRoot);

